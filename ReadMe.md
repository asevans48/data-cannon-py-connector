# RabbitMQ Python Connector to the DataCannon System in Rust

Connects to the Data Cannon RS environment from Python. The RS environment works for networking and has tools
for processing. However, Python is better a Object Oriented work and makes working with data easy.

# Features

Connects sources and sinks via their respective package and source.

A typical connector will be as in a folder as follows:

```
     sources/
          rabbitmq/
            publisher
     sinks/
         rabbitmq/
            subscriber
```

# License

See the license section
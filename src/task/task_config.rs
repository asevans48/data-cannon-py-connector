//! A task configuration object


use crate::arg::argtype::DCArgType;
use std::collections::HashMap;

#[pyclass]
pub struct PyTaskConfig{
    broker: Option<String>,
    task_name: Option<String>,
    args: Option<Vec<DCArgType>>,
    kwargs: Option<HashMap<String, DCArgType>>,
    reply_to: Option<String>,
    parent_id: Option<String>,
    correlation_id: Option<String>,
    result_expires: Option<u64>,
    priority: Option<i8>,
    time_limit: Option<u64>,
    soft_time_limit: Option<u64>,
    eta: Option<u64>,
    retries: Option<u8>,
    lang: Option<String>,
    shadow: Option<String>,
    exchange: Option<String>,
    exchange_type: Option<String>,
    routing_key: Option<String>
}


#[pymethods]
impl PyTaskConfig{

    #[new]
    pub fn new() -> Self{
        PyTaskConfig{
            broker: None,
            task_name: None,
            args: None,
            kwargs: None,
            reply_to: None,
            parent_id: None,
            correlation_id: None,
            result_expires: None,
            priority: None,
            time_limit: None,
            soft_time_limit: None,
            eta: None,
            retries: None,
            lang: None,
            shadow: None,
            exchange: None,
            exchange_type: None,
            routing_key: None
        }
    }
}

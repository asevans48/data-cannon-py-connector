pub mod worker;
pub mod client;
pub mod config;
pub mod arg;
pub mod task;
pub mod stream;


use pyo3::prelude::*;
use crate::config::hapolicies::{PyRabbitMQHAPolicy, PyHAPolicy};
use crate::config::exchange::{PyExchangeKind, PyExchange};
use crate::config::backend::PyBackendConfig;
use crate::config::backend::PyBackendType;
use crate::config::queue::{PyQueuePersistenceType, PyAMQPQueue};
use crate::config::connection::PyAMQPConnection;
use crate::config::broker::PyBrokerType;
use crate::config::routers::PyRouter;


#[pymodule]
fn datacannonclient(_: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyAMQPConnection>().unwrap();
    m.add_class::<PyAMQPQueue>().unwrap();
    m.add_class::<PyRabbitMQHAPolicy>().unwrap();
    m.add_class::<PyExchangeKind>().unwrap();
    m.add_class::<PyBackendConfig>().unwrap();
    m.add_class::<PyBackendType>().unwrap();
    m.add_class::<PyBrokerType>().unwrap();
    m.add_class::<PyQueuePersistenceType>().unwrap();
    m.add_class::<PyExchange>().unwrap();
    m.add_class::<PyRouter>().unwrap();
    m.add_class::<PyRouters>().unwrap();
    m.add_class::<PyHAPolicy>().unwrap();
    m.add_class::<PyConectionConfig>().unwrap();
    Ok(())
}

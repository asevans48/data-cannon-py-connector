//! Stores Rust argtypes


use pyo3::{PyObject, Python};
use datacannon_rs_core::argparse::argtype::ArgType;


#[pyclass]
pub struct DCArgMap{

}


#[pymethods]
impl DCArgMap{

}


#[pyclass]
pub struct DCArgTypeVec{

}


#[pymethods]
impl DCArgTypeVec{

}


#[pyclass]
pub struct DCArgType{
    argtype: Option<ArgType>
}


#[pymethods]
impl DCArgType{


    /// Constructor
    pub fn new() -> Self{
        DCArgType{
            argtype: None
        }
    }
    
}

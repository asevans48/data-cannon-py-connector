//! Stream configuration information


use datacannon_rs_core::message_protocol::stream::StreamConfig;
use crate::task::task_config::PyTaskConfig;
use pyo3::PyResult;


#[pyclass]
pub struct PyStreamConfig{
    chord: Vec<PyTaskConfig>,
    chain: Vec<PyStreamConfig>
}


#[pymethods]
impl PyStreamConfig{

    /// Ad d a task to the chord
    ///
    /// # Arguments
    /// * `task` - Task to run in parallel
    pub fn add_to_chord(&mut self, task: PyTaskConfig) -> PyResult<()>{
        self.chord.push(task);
        Ok(())
    }

    /// Chain another set of tasks to the configuration
    ///
    /// # Arguments
    /// * `stream_config` - Configuration to stream
    pub fn chain(&mut self, stream_config: PyStreamConfig) -> PyResult<()>{
        self.chain.push(stream_config);
        Ok(())
    }

    ///Constructor
    #[new]
    pub fn new() -> Self{
        PyStreamConfig{
            chord: vec![],
            chain: vec![]
        }
    }
}


/// Converts a PythonStreamConfig to a StreamConfig
/// TODO Finish conversion
pub(crate) fn to_stream_config(py_stream_config: &PyStreamConfig) -> StreamConfig{
    StreamConfig{
        chord: vec![],
        chain: vec![]
    }
}

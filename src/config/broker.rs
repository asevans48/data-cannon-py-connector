//! Broker configuration

use pyo3::prelude::*;
use pyo3::PyResult;
use datacannon_rs_core::config::config::BrokerType;

#[pyclass]
pub struct PyBrokerType{
    broker_type: String
}


#[pymethods]
impl PyBrokerType{

    /// Sets the broker type to RabbitMQ
    pub fn rabbitmq(&mut self) -> PyResult<()>{
        self.broker_type = "RABBITMQ".to_string();
        Ok(())
    }

    /// Constructor
    #[new]
    pub fn new() -> Self{
        PyBrokerType{
            broker_type: "RABBITMQ".to_string()
        }
    }
}


/// Converts the python broker type to a rust broker type
///
/// # Arguments
/// * `broker_type` - The Python object
pub fn to_broker_type(broker_type: &PyBrokerType) -> BrokerType{
    let btype = broker_type.broker_type.clone();
    let bref = btype.as_str();
    let btype = match bref{
        "RABBITMQ" => {
            BrokerType::RABBITMQ
        },
        _ => {
            BrokerType::RABBITMQ
        }
    };
    btype
}
//! Connection information for each type of connection

use pyo3::prelude::*;
use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;


#[pyclass]
pub struct PyConnectionConfig{
    rabbitmq: Option<PyAMQPConnection>
}


#[pymethods]
impl PyConnectionConfig{

    /// Set a RabbitMQ Connection
    ///
    /// # Arguments
    /// * `connection_inf` - Connection information
    pub fn rabbitmq(&mut self, connection_inf: PyAMQPConnection) -> PyResult<()>{
        self.rabbitmq = Some(connection_inf);
        Ok(())
    }

    /// Constructor
    #[new]
    pub fn new() -> Self{
        PyConnectionConfig{
            rabbitmq: None
        }
    }
}


#[pyclass]
#[derive(Clone, Debug)]
pub struct PyAMQPConnection{
    protocol: String,
    host: String,
    port: i64,
    vhost: Option<String>,
    username: Option<String>,
    password: Option<String>,
    connection_timeout: u64
}


#[pymethods]
impl PyAMQPConnection{

    pub fn password(&mut self, password: String) -> PyResult<()>{
        self.password = Some(password);
        Ok(())
    }

    pub fn username(&mut self, username: String) -> PyResult<()>{
        self.username = Some(username);
        Ok(())
    }

    pub fn vhost(&mut self, vhost: String) -> PyResult<()>{
        self.vhost = Some(vhost);
        Ok(())
    }

    #[new]
    pub fn new(protocol: String, host: String, port: i64, connection_timeout: u64) -> Self{
        PyAMQPConnection{
            protocol,
            host,
            port,
            vhost: None,
            username: None,
            password: None,
            connection_timeout
        }
    }
}


/// Convert AMQP connection information to connection information
///
/// # Arguments
/// * `connection` - Python AMQP Connection
pub(crate) fn to_amqp_connection_inf(connection: &PyAMQPConnection) -> AMQPConnectionInf{
    AMQPConnectionInf::new(
        connection.protocol.clone(),
        connection.host.clone(),
        connection.port,
        connection.vhost.clone(),
        connection.username.clone(),
        connection.password.clone(),
        connection.connection_timeout)
}

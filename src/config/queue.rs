//! Queue configuration. Applies to message queueing brokers with this option.

use pyo3::prelude::*;
use pyo3::PyResult;
use datacannon_rs_core::config::config::QueuePersistenceType;
use crate::config::connection::{PyAMQPConnection, to_amqp_connection_inf};
use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
use crate::config::hapolicies::{PyRabbitMQHAPolicy, to_ha_policy};
use datacannon_rs_core::replication::replication::HAPolicy;
use datacannon_rs_core::replication::rabbitmq::RabbitMQHAPolicy;


#[pyclass]
#[derive(Clone, Debug)]
pub struct PyAMQPQueue{
    name: String,
    exchange: Option<String>,
    routing_key: Option<String>,
    max_priority: i8,
    ha_policy: PyRabbitMQHAPolicy,
    conn_inf: PyAMQPConnection,
    is_durable: bool,
    prefetch_count: u32
}


#[pymethods]
impl PyAMQPQueue{

    /// Constructor
    ///
    /// # Arguments
    /// * `name` - Name of the queue
    /// * `exchange` - Optional name of the exchange
    /// * `routing_key` - Optional name of the routing key
    /// * `max_priority` - Optional mmaximum priority for the queue
    /// * `conn_inf` - Conection Information
    #[new]
    pub fn new(
        name: String,
        exchange: Option<String>,
        routing_key: Option<String>,
        max_priority: i8,
        conn_inf: PyAMQPConnection,
        ha_policy: PyRabbitMQHAPolicy) -> Self{
        PyAMQPQueue{
            name,
            exchange,
            routing_key,
            max_priority: max_priority,
            ha_policy: ha_policy,
            conn_inf,
            is_durable: false,
            prefetch_count: 0
        }
    }
}


#[pyclass]
pub struct PyQueuePersistenceType{
    persistence_type: String
}


#[pymethods]
impl PyQueuePersistenceType{

    /// Set the persistence type to non-persistent
    pub fn non_persistent(&mut self) -> PyResult<()>{
        self.persistence_type = "NONPERSISTENT".to_string();
        Ok(())
    }

    /// Set the Queue persistence type to persistent
    pub fn persistent(&mut self) -> PyResult<()>{
        self.persistence_type = "PERSISTENT".to_string();
        Ok(())
    }

    /// Constructor for the function
    #[new]
    pub fn new() -> Self{
        PyQueuePersistenceType{
            persistence_type: "PERSISTENT".to_string()
        }
    }
}


/// Converts a PythonQueue Persistence object to its corresponding Rust Object
///
/// # Arguments
/// * `py_persistence_type` - Python persistence object
pub(crate) fn to_queue_persistence_type(py_persistence_type: &PyQueuePersistenceType) -> QueuePersistenceType{
    let persistence_type = py_persistence_type.persistence_type.clone();
    let pers_ref = persistence_type.as_str();
    match pers_ref {
        "PERSISTENT" =>{
            QueuePersistenceType::PERSISTENT
        },
        "NONPERSISTENT" => {
            QueuePersistenceType::NONPERSISTENT
        },
        _ => {
            QueuePersistenceType::PERSISTENT
        }
    }
}


/// Convert the Python Queue to a RabbitMQ queue
///
/// # Arguments
/// * `queue` - Python queue
pub(crate) fn to_amqp_queue(queue: &PyAMQPQueue)  -> AMQPQueue{
    let rmq_ha_policy = to_ha_policy(&queue.ha_policy);
    let policy = HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(rmq_ha_policy, 1));
    let conn_inf = to_amqp_connection_inf(&queue.conn_inf);
    AMQPQueue::new(queue.name.clone(),
                   queue.exchange.clone(),
                   queue.routing_key.clone(),
                   queue.max_priority,
                   policy,
                   queue.is_durable,
                   conn_inf)
}

//!Routers and router object corresponding to the Rust configuration

use crate::config::exchange::{PyExchange, to_exchange};

use pyo3::prelude::*;
use datacannon_rs_core::router::router::Router;
use crate::config::queue::{PyAMQPQueue, to_amqp_queue};
use datacannon_rs_core::message_structure::queues::GenericQueue;


/// Broker router configuration. Brokers must use queues. Exchanges are applicable for AMQP directly
/// and Kafka through code.
#[pyclass]
#[derive(Clone, Debug)]
pub struct PyRouter{
    routing_key: String,
    queues: Vec<PyObject>,
    exchange: Option<PyExchange>
}


#[pymethods]
impl PyRouter{

    /// Add the queue to the quues list
    ///
    /// # Arguments
    /// * `queue` - Queue to append to
    pub fn add_queue(&mut self, queue: PyObject) -> PyResult<()> {
        self.queues.push(queue);
        Ok(())
    }

    /// Create a new PyRouter object
    ///
    /// # Arguments
    /// * `routing_key` - Routing key for the queue. In Kafka, this would be handled in the code.
    /// * `exchange` - Exchange if applicable
    #[new]
    pub fn new(routing_key: String, exchange: Option<PyExchange>) -> Self {
        PyRouter{
            routing_key,
            queues: vec![],
            exchange
        }
    }
}


/// Converts the Python router to a Rust router
///
/// # Arguments
/// * `router` - Python router
pub(crate) fn to_router(py: Python, router: &PyRouter) -> Router {
    let mut queues = vec![];
    let exchange = {
        if router.exchange.is_some() {
            let inner_exchange = router.exchange.clone().unwrap();
            let ex = to_exchange(&inner_exchange);
            Some(ex)
        }else{
            None
        }
    };
    for queue in &router.queues{
        let amqp_queue = queue.extract::<PyAMQPQueue>(py);
        if amqp_queue.is_ok(){
            let pyqueue = amqp_queue.unwrap();
            let queue = to_amqp_queue(&pyqueue);
            queues.push(GenericQueue::AMQPQueue(queue))
        }
    }
    Router::new(router.routing_key.clone(), queues, exchange)
}

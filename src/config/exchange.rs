//! Python class for the exchange

use lapin::ExchangeKind;

use pyo3::prelude::*;
use pyo3::PyResult;
use datacannon_rs_core::message_structure::amqp::exchange::Exchange;


#[pyclass]
#[derive(Clone, Debug)]
pub struct PyExchangeKind{
    exchange_type: String
}


#[pymethods]
impl PyExchangeKind{

    pub fn fanout(&mut self) -> PyResult<()>{
        self.exchange_type = "FANOUT".to_string();
        Ok(())
    }

    pub fn headers(&mut self) -> PyResult<()>{
        self.exchange_type = "HEADERS".to_string();
        Ok(())
    }

    pub fn topic(&mut self) -> PyResult<()> {
        self.exchange_type = "TOPIC".to_string();
        Ok(())
    }

    pub fn direct(&mut self) -> PyResult<()>{
        self.exchange_type =  "DIRECT".to_string();
        Ok(())
    }

    pub fn set_exchange_type(&mut self, exchange_type: String) -> PyResult<()>{
        self.exchange_type = exchange_type;
        Ok(())
    }

    pub fn get_exchange_type(&mut self) -> PyResult<String> {
        let exchange_kind = self.exchange_type.clone();
        Ok(exchange_kind)
    }

    #[new]
    pub fn new() -> Self{
        PyExchangeKind{
            exchange_type: "DIRECT".to_string()
        }
    }
}


#[pyclass]
#[derive(Clone, Debug)]
pub struct PyExchange{
    name: String,
    exchange: PyExchangeKind
}


#[pymethods]
impl PyExchange{

    #[new]
    pub fn new(name: String, exchange: PyExchangeKind) -> Self {
        PyExchange{
            name,
            exchange
        }
    }
}


/// Convert a Python exchange type to a rust exchange type
///
/// # Arguments
/// * `py_exchange_kind` - Python exchange type
pub fn to_exchange_type(py_exchange_kind: &PyExchangeKind) -> ExchangeKind{
    let exchange_type = py_exchange_kind.exchange_type.clone();
    let exchange_ref = exchange_type.as_str();
    match exchange_ref {
        "DIRECT" => {
            ExchangeKind::Direct
        },
        "FANOUT" => {
            ExchangeKind::Fanout
        },
        "HEADERS" => {
            ExchangeKind::Headers
        },
        "TOPIC" => {
            ExchangeKind::Topic
        },
        _ => {
            ExchangeKind::Custom(exchange_type)
        }
    }
}


/// Convert the Python exchange object to a Rust exchange object
///
/// # Arguments
/// * `exchange` - Python exchange object
pub fn to_exchange(exchange: &PyExchange) -> Exchange {
    let exchange_kind = to_exchange_type(&exchange.exchange);
    Exchange::new(exchange.name.clone(), exchange_kind)
}


#[cfg(test)]
mod tests{

    use crate::config::exchange::{PyExchangeKind, to_exchange_type};
    use lapin::ExchangeKind;

    #[test]
    fn test(){
        let exchange = PyExchangeKind::new();
        let ekind = to_exchange_type(&exchange);
        if let ExchangeKind::Direct = ekind{

        }else{
            assert!(false)
        }
    }
}
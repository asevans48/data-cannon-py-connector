//! Cannon Configuration

use pyo3::prelude::*;
use crate::config::connection::PyConnectionConfig;
use crate::config::backend::{PyBackendType, PyBackendConfig};
use crate::config::exchange::PyExchangeKind;
use crate::config::queue::PyQueuePersistenceType;
use crate::config::hapolicies::PyHAPolicy;
use pyo3::types::PyDict;
use std::collections::HashMap;
use crate::arg::argtype::DCArgType;


#[pyclass]
pub struct PyCannonConfig{
    pub connection_inf: PyConnectionConfig,
    pub event_backend_type: PyBackendType,
    pub event_backend: PyBackendConfig,
    pub default_backend: PyBackendType,
    pub routers: PyRouters,
    pub cache_backend: Option<PyBackendConfig>,
    pub send_events: bool,
    pub default_exchange: String,
    pub default_exchange_type: PyExchangeKind,
    pub default_queue: String,
    pub event_routing_key: String,
    pub result_exchange: String,
    pub accept_content: String,
    pub worker_prefetch_multiplier: i8,
    pub default_delivery_mode: PyQueuePersistenceType,
    pub default_routing_key: String,
    pub broker_connection_timeout: i64,
    pub broker_connection_max_retries: i64,
    pub acks_late: bool,
    pub task_result_expires: i64,
    pub ignore_result: bool,
    pub max_cached_results: i32,
    pub result_persistent: PyQueuePersistenceType,
    pub result_serializer: String,
    pub database_engine_options: Option<PyDict>,
    pub num_broker_connections: u32,
    pub num_broker_channels: u32,
    pub num_broker_threads: u32,
    pub num_backend_connections: u32,
    pub num_backend_channels: u32,
    pub num_backend_threads: u32,
    pub ha_policy: Option<PyHAPolicy>,
    pub create_missing_queues: bool,
    pub broker_transport_options: Option<HashMap<String, DCArgType>>,
    pub task_queue_max_priority: Option<i8>,
    pub task_default_priority: i8,
    pub maximum_allowed_failures: u8,
    pub maximum_allowed_failures_per_n_calls: u8,
    pub task_retries: u8,
    pub default_lang: String,
    pub app_lang: String,
    pub encoding_type: String,
    pub message_size: usize,
    pub drop_queues_on_close: bool,
    pub purge_queues_on_start: bool,
    pub drop_exchanges_on_close: bool,
    pub prefetch_limit: usize,
    pub max_concurrent_tasks:usize,
    pub consumers_per_queue: usize
}

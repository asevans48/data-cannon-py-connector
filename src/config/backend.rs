//! Backend configuration

use pyo3::prelude::*;
use pyo3::PyResult;
use datacannon_rs_core::config::config::BackendType;


#[pyclass]
pub struct PyBackendType{
    backend_type: String
}


#[pymethods]
impl PyBackendType{

    /// Sets the backend tye to Redis
    pub fn redis(&mut self) -> PyResult<()>{
        self.backend_type = "REDIS".to_string();
        Ok(())
    }

    /// Constructor for the backend type
    #[new]
    pub fn new() -> Self{
        PyBackendType{
            backend_type: "REDIS".to_string()
        }
    }
}


#[pyclass]
pub struct PyBackendConfig{
    pub url: String,
    pub username: Option<String>,
    pub password: Option<String>,
    pub transport_options: Option<String>
}


#[pymethods]
impl PyBackendConfig{

    /// Create a new Backend Configuration
    ///
    /// # Arguments
    /// - `url` -   URL for the backend
    /// - `username` - Optional username
    /// - `password` - Optional password parameter
    /// - `transport_options` - Optional transport options if supported
    #[new]
    pub fn new(
        url: String,
        username: Option<String>,
        password: Option<String>,
        transport_options: Option<String>) -> Self{
        PyBackendConfig{
            url,
            username,
            password,
            transport_options
        }
    }
}


/// Converts a PyBackendType to a Rust backend type
///
/// # Arguents
/// * `backend_type` - PyBackendType to convert
pub fn to_backend_type(backend_type: &PyBackendType) -> BackendType{
    let backend_cfg = backend_type.backend_type.clone();
    let backend_ref = backend_cfg.as_str();
    match backend_ref  {
        "REDIS" => {
            BackendType::REDIS
        },
        _ => {
            BackendType::REDIS
        }
    }
}

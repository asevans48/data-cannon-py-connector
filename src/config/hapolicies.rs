//! Deals with HA Policies

use pyo3::prelude::*;
use pyo3::types::PyList;
use datacannon_rs_core::replication::rabbitmq::RabbitMQHAPolicies;


#[pyclass]
#[derive(Clone, Debug)]
pub struct PyRabbitMQHAPolicy{
    hapolicy: String,
    nodes: Vec<String>,
    exactly: i8
}


#[pymethods]
impl PyRabbitMQHAPolicy{

    /// Set the HA Policy to one replicating the provided number of nodes
    ///
    /// # Arguments
    /// * `num_nodes` - The number of nodes to replicate to
    pub fn exactly(&mut self, num_nodes: i8) -> PyResult<()>{
        self.exactly = num_nodes;
        self.hapolicy = "EXACTLY".to_string();
        Ok(())
    }

    /// Set the HA Policy to use an exact list of node names
    ///
    /// # Arguments
    /// * `names` - Names of the nodes to replicate across
    pub fn named_nodes(&mut self, names: &PyList) -> PyResult<()>{
        self.hapolicy = "NODES".to_string();
        let name_vec = names.extract().unwrap();
        self.nodes = name_vec;
        Ok(())
    }

    /// Set the HA Policy to replicate across all nodes
    pub fn all(&mut self) -> PyResult<()> {
        self.hapolicy = "ALL".to_string();
        Ok(())
    }

    #[new]
    pub fn new() -> Self{
        PyRabbitMQHAPolicy{
            hapolicy: "ALL".to_string(),
            nodes: vec![],
            exactly: 1
        }
    }
}


#[pyclass]
pub struct PyHAPolicy{
    rabbitmq: Option<RabbitMQHAPolicies>
}


#[pymethods]
impl PyHAPolicy{

    /// Specify a RabbitMQ HA Policy
    pub fn rabbitmq(&mut self, policy: RabbitMQHAPolicies) -> PyResult<()>{
        self.rabbitmq = Some(policy);
        Ok(())
    }

    /// Constructor
    #[new]
    pub fn new() -> Self{
        PyHAPolicy{
            rabbitmq: None
        }
    }
}


pub(crate) fn to_ha_policy(policy: &PyRabbitMQHAPolicy) -> RabbitMQHAPolicies{
    if policy.hapolicy.eq("ALL"){
        RabbitMQHAPolicies::ALL
    }else if policy.hapolicy.eq("EXACTLY"){
        RabbitMQHAPolicies::EXACTLY(policy.exactly)
    }else{
        RabbitMQHAPolicies::NODES(policy.nodes.clone())
    }
}


#[cfg(test)]
mod tests{
    use datacannon_rs_core::replication::rabbitmq::RabbitMQHAPolicies;
    use crate::config::hapolicies::{PyRabbitMQHAPolicy, to_ha_policy};

    #[test]
    fn test_convert_to_ha_policy(){
        let py_policy = PyRabbitMQHAPolicy{
            hapolicy: "ALL".to_string(),
            nodes: vec![],
            exactly: 0
        };
        let rust_policy = to_ha_policy(&py_policy);
        if let RabbitMQHAPolicies::ALL = rust_policy {

        }else{
            assert!(false);
        }
    }
}
